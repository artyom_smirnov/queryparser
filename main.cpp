#include <iostream>

#include "Driver.h"
#include "ParsingException.h"

using namespace std;
using namespace ParserExample;

int main()
{
    try
    {
        Driver d(true, true);
        d.parse("SELECT FROM");
    }
    catch(const ParsingException& e)
    {
        cerr
            << "Parsing failed:" << endl
            << e.what() << endl;
    }
}
