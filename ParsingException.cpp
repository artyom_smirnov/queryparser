/**
 * @file
 *
 * @author Artyom Smirnov <smirnoffjr@gmail.com>
 *
 * @brief Simple exception for throwing/catching parser errors
 */

#include "ParsingException.h"

namespace ParserExample
{

ParsingException::ParsingException(const std::string& message):
    _message(message)
{
}

const char* ParsingException::what() const throw()
{
    return _message.c_str();
}

}
