/**
 * @file
 *
 * @author Artyom Smirnov <smirnoffjr@gmail.com>
 *
 * @brief Grammar for query parser
 */

%skeleton "lalr1.cc"
%require "2.4"
%defines
%define parser_class_name "Parser"
%debug
%error-verbose
%verbose
%expect 0

%code requires
{
    #include <string>
    namespace ParserExample
    {
        class Driver;
        class ASTNode;
    }
}

%parse-param { ParserExample::Driver& driver }
//%lex-param   { ParserExample::Driver& driver }

%locations
%initial-action
{
};

%union
{
    std::string     *sval;
    ParserExample::ASTNode *astNode;
};

%code
{
    #include "Driver.h"

    using namespace ParserExample;

    #define LOC(tok) {tok.begin.line, tok.begin.column, tok.end.line, tok.end.column}
}

%token SELECT FROM

%token        EOQ      0  "end of query"
%token <sval> IDENTIFIER  "identifier"
%token <sval> INTEGER     "integer constant"
%token <sval> FLOAT       "float constant"
%token <sval> STRING      "string constant"
%token <sval> BOOLEAN     "boolean constant"

%token GTEQ ">="
%token NEQ "!="
%token LSEQ "<="

%left OR
%left AND
%right NOT
%left CMP NEQ '>' GTEQ '<' LSEQ
%left '+' '-'
%left '*' '/' '%'
%left UNARY_MINUS UNARY_PLUS

%type <astNode> statement select_statement selection_list relation_name identifier

%printer    { debug_stream () << *$$; } <sval>

%destructor { delete $$; $$ = nullptr; } <sval> <astNode>

%start start;

%%

start:
    statement
    {
        driver._result = $1;
    }
    ;

statement:
    select_statement
    ;

select_statement:
    SELECT selection_list FROM relation_name
    {
        $$ = new ASTNode(node_select, LOC(@$));
    }
    ;

selection_list:
    {
        $$ = nullptr;
    }
    ;

relation_name:
    identifier
    {
        $$ = $1;
    }
    ;

identifier:
    IDENTIFIER
    {
        $$ = new ASTNode(node_identifier, LOC(@$));
        delete $1;
    }
    ;

%%

void yy::Parser::error(const yy::Parser::location_type& l, const std::string& m)
{
    driver.error({l.begin.line, l.begin.column, l.end.line, l.end.column}, m);
}
