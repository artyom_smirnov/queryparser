/**
 * @file
 *
 * @author Artyom Smirnov <smirnoffjr@gmail.com>
 *
 * @brief Structure describes location of AST node in query string 
 */

#ifndef QUERYLOCATION_H_
#define QUERYLOCATION_H_

#include <stdint.h>

 /**
 * @brief Structure describes location of AST node in query string
 */
struct QueryLocation
{
    /**
     * Line of start position
     */
    uint32_t beginLine;

    /**
     * Column of start position
     */
    uint32_t beginCol;

    /**
     * Line of end position
     */
    uint32_t endLine;

    /**
     * Column of end position
     */
    uint32_t endCol;
};

#endif //QUERYLOCATION_H_
