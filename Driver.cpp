/**
 * @file
 *
 * @author Artyom Smirnov <smirnoffjr@gmail.com>
 *
 * @brief Driver to launch lexer and parser
 */

#include <assert.h>

#include "Driver.h"
#include "Parser.hpp"
#include "ParsingException.h"

namespace ParserExample
{

Driver::Driver(bool traceParsing, bool traceScanning):
    _traceParsing(traceParsing),
    _traceScanning(traceScanning),
    _result(nullptr)
{
}

void Driver::parse(const std::string &queryString)
{
    _queryString = queryString;
    scan_begin();
    yy::Parser parser(*this);
    parser.set_debug_level(_traceParsing);
    int res = parser.parse();
    scan_end();

    if (res)
    {
        throw ParsingException(_errorMessage);
    }
}

void Driver::error(const QueryLocation& errorLocation, const std::string& errorMessage)
{
    _errorMessage = errorMessage;
    _errorLocation = errorLocation;
}

void Driver::error(const std::string& errorMessage)
{
    _errorMessage = errorMessage;
}

ASTNode* Driver::getResult() const
{
    return _result;
}

const std::string& Driver::getErrorMessage() const
{
    return _errorMessage;
}

const QueryLocation& Driver::getErrorLocation() const
{
    return _errorLocation;
}

} //namespace ParserExample
