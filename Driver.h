/**
 * @file
 *
 * @author Artyom Smirnov <smirnoffjr@gmail.com>
 *
 * @brief Driver to launch lexer and parser
 */

#ifndef QUERY_PARSER_H_
#define QUERY_PARSER_H_
#include <string>

#include "Parser.hpp"
#include "AST.h"

#define YY_DECL                             \
  yy::Parser::token_type                    \
  yylex (yy::Parser::semantic_type* yylval, \
         yy::Parser::location_type* yylloc)

YY_DECL;

namespace ParserExample
{

class Driver
{
public:
    Driver(bool traceParsing, bool traceScanning);

    void parse(const std::string& queryString);

    ASTNode* getResult() const;

    const std::string& getErrorMessage() const;

    const QueryLocation& getErrorLocation() const;

    void scan_begin();

    void scan_end();

    void error(const QueryLocation& errorLocation, const std::string& errorMessage);

    void error(const std::string& errorMessage);


private:
    std::string _queryString;

    bool _traceParsing;

    bool _traceScanning;

    ASTNode* _result;

    std::string _errorMessage;

    QueryLocation _errorLocation;

    friend class yy::Parser;
};

} //namespace ParserExample

#endif
