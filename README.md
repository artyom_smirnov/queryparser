# Required build dependencies

* cmake
* g++ >= 4.7
* flex >= 2.5.35
* bison >= 2.4

# Build

1. cmake .
2. make
3. make install
