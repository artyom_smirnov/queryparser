/**
 * @file
 *
 * @author Artyom Smirnov <smirnoffjr@gmail.com>
 *
 * @brief Lexems for query parser
 */

%{
    #include <string>

    #include "Driver.h"
    #include "Parser.hpp"

    #define yyterminate() return token::EOQ

    struct Keyword
    {
        const char* name;
        yy::Parser::token::yytokentype tok;
    };

    const Keyword Keywords[] =
    {
        {"select", yy::Parser::token::SELECT},
        {"from", yy::Parser::token::FROM},
        {nullptr,  yy::Parser::token::EOQ}
    };

    static const Keyword* CheckKeyword(const char *keyword);

    static void unescapeStringLiteral(std::string &s);
%}

%option noyywrap nounput batch debug

Id      [a-zA-Z][a-zA-Z_0-9]*

Digit   [0-9]
Int     {Digit}+
Decimal (({Digit}*\.{Digit}+)|({Digit}+\.{Digit}*))
Real    ({Int}|{Decimal})[Ee][-+]?{Digit}+

Blank [ \t]

%{
    #define YY_USER_ACTION yylloc->columns(yyleng);
%}

%%

%{
    yylloc->step();
%}

{Blank}+   yylloc->step();
[\n]+      yylloc->lines(yyleng); yylloc->step();

%{
    typedef yy::Parser::token token;
%}

"==" {
    return token::CMP;
}

"<=" {
    return token::LSEQ;
}

"!=" {
    return token::NEQ;
}

">=" {
    return token::GTEQ;
}

(?i:not) {
    return token::NOT;
}

(?i:and) {
    return token::AND;
}

(?i:or) {
    return token::OR;
}

{Int} {
    yylval->sval = new std::string(yytext);;
    return token::INTEGER;
}

{Decimal}|{Real} {
    yylval->sval = new std::string(yytext);
    return token::FLOAT;
}

(?i:true)|(?i:false) {
    yylval->sval = new std::string(yytext);
    return token::BOOLEAN;
}

L?\'(\\.|[^\\\'])*\' {
    std::string *str = new std::string(yytext, 1, yyleng - 2);
    unescapeStringLiteral(*str);
    yylval->sval = str;
    return token::STRING;
}

{Id} {
    const Keyword *kw = CheckKeyword(yytext);
    if (kw)
        return kw->tok;
    yylval->sval = new std::string(yytext);
    return token::IDENTIFIER;
}

. {
    return yy::Parser::token_type (yytext[0]);
}

%%

void ParserExample::Driver::scan_begin()
{
    yy_flex_debug = _traceScanning;
    yy_scan_string(_queryString.c_str());
}

void ParserExample::Driver::scan_end()
{
}

static const Keyword* CheckKeyword(const char *keyword)
{
    const Keyword *kw = &Keywords[0];

    while(kw->name)
    {
        if (!strcasecmp(kw->name, keyword))
            return kw;
        ++kw;
    }

    return nullptr;
}

static void unescapeStringLiteral(std::string &s)
{
    size_t slen = s.size();
    char prevChar = 0;
    char currChar = 0;
    for(size_t pos = 0; pos < slen; ++pos)
    {
        currChar = s[pos];

        if (pos != 0)
        {
            if (prevChar == '\\' && currChar == '\'')
            {
                s.erase(pos - 1, 1);
                --pos;
                --slen;
            }
        }

        prevChar = currChar;
    }
}
