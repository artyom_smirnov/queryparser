/**
 * @file
 *
 * @author Artyom Smirnov <smirnoffjr@gmail.com>
 *
 * @brief Abstract Syntax Tree nodes
 */

#ifndef AST_H_
#define AST_H_

#include <string> 
#include <vector>

#include "QueryLocation.h"

namespace ParserExample
{

/**
 * All known AST nodes
 */
typedef enum
{
    node_unknown = 0,
    node_select,
    node_constant,
    node_identifier
} NodeType;

/**
 * All supported AST constants
 */
typedef enum
{
    intVal = 0,
    floatVal,
    stringVal,
    boolVal
} NodeConstantType;

/**
 * Generic AST node
 */
class ASTNode
{
public:
    NodeType getType() const;

    const QueryLocation& getNodeLocation() const;

    void addChild(ASTNode* childNode);

    const std::vector<ASTNode*>& getChilds() const;

    ASTNode* getChild(uint32_t childNo) const;

public:
    ASTNode(NodeType nodeType, const QueryLocation& nodeLocation);

    ASTNode(NodeType nodeType, ASTNode* childNode, const QueryLocation& nodeLocation);

    ASTNode(NodeType nodeType, std::initializer_list<ASTNode*> childNodes, const QueryLocation& nodeLocation);

    ~ASTNode();

private:
    NodeType _nodeType;

    std::vector<ASTNode*> _childNodes;

    QueryLocation _nodeLocation;
};

/**
 * Constant value node
 */
class ASTNodeConstant: public ASTNode
{
public:
    NodeConstantType getConstantType() const;

    const std::string& getVal() const;

public:
    ASTNodeConstant(NodeConstantType constantType, const std::string &val, const QueryLocation& nodeLocation);

private:
    NodeConstantType _constantType;

    std::string _val;
};

} //namespace ParserExample

#endif // AST_H_
