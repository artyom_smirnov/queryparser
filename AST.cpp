/**
 * @file
 *
 * @author Artyom Smirnov <smirnoffjr@gmail.com>
 *
 * @brief Abstract Syntax Tree nodes
 */

#include "AST.h"

using namespace std;

namespace ParserExample
{

ASTNode::ASTNode(NodeType nodeType, const QueryLocation& nodeLocation):
    _nodeType(nodeType),
    _childNodes({}),
    _nodeLocation(nodeLocation)
{

}

ASTNode::ASTNode(NodeType nodeType, ASTNode* childNode, const QueryLocation& nodeLocation):
    _nodeType(nodeType),
    _childNodes({}),
    _nodeLocation(nodeLocation)
{
    _childNodes.push_back(childNode);
}

ASTNode::ASTNode(NodeType nodeType, initializer_list<ASTNode*> childNodes, const QueryLocation& nodeLocation):
    _nodeType(nodeType),
    _childNodes(childNodes),
    _nodeLocation(nodeLocation)
{
}

void ASTNode::addChild(ASTNode *childNode)
{
    _childNodes.push_back(childNode);
}

ASTNode::~ASTNode()
{
    for(ASTNode* child: _childNodes)
    {
		delete child;
		child = nullptr;
    }
    _childNodes.clear();
}

const std::vector<ASTNode*>& ASTNode::getChilds() const
{
    return _childNodes;
}

ASTNode* ASTNode::getChild(uint32_t childNo) const
{
    return _childNodes[childNo];
}

NodeType ASTNode::getType() const
{
    return _nodeType;
}

const QueryLocation& ASTNode::getNodeLocation() const
{
    return _nodeLocation;
}

ASTNodeConstant::ASTNodeConstant(NodeConstantType constantType, const std::string &val, const QueryLocation& nodeLocation):
    ASTNode(node_constant, nodeLocation),
    _constantType(constantType),
    _val(val)
{
}

NodeConstantType ASTNodeConstant::getConstantType() const
{
    return _constantType;
}

const std::string& ASTNodeConstant::getVal() const
{
    return _val;
}

} //namespace ParserExample
