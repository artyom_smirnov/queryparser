/**
 * @file
 *
 * @author Artyom Smirnov <smirnoffjr@gmail.com>
 *
 * @brief Simple exception for throwing/catching parser errors
 */

#include <exception>
#include <string>

namespace ParserExample
{

class ParsingException: public std::exception
{
public:
    ParsingException(const std::string& message);

    const char* what() const throw();

    ~ParsingException() throw() {}

private:
    std::string _message;
};

}
